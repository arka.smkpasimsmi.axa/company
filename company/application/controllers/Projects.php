<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Projects extends CI_Controller
{

    function __construct() {
        parent:: __construct();
        $this->load->model('m_projects');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['project'] = $this->m_projects->tampil_data()->result();
        $data['autonumber'] = $this->m_projects->auto_number();
        $data['namaperusahaan'] = $this->m_projects->tambah_data()->result();

        $this->load->view('template/headercss');
        $this->load->view('template/sidebar');
        $this->load->view('template/navbar');
        $this->load->view('project/index', $data);
        $this->load->view('template/footerjs');
    }


    public function tambah_aksi()
    {
        $this->form_validation->set_rules('dname', 'name', 'required');
        $this->form_validation->set_rules('ddesc', 'description', 'required');
        $this->form_validation->set_rules('ddate1', 'date1', 'required');
        $this->form_validation->set_rules('dtime1', 'time1', 'required');
        $this->form_validation->set_rules('ddate2', 'date2', 'required');
        $this->form_validation->set_rules('dtime2', 'time2', 'required');
        $this->form_validation->set_rules('dstatus', 'status', 'required');

        if ($this->form_validation->run() == false) {
            $data['project'] = $this->m_projects->tampil_data()->result();
            $data['autonumber'] = $this->m_projects->auto_number();
            $data['namaperusahaan'] = $this->m_projects->tambah_data()->result();

            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Fail!
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>');
            $this->load->view('template/headercss');
            $this->load->view('template/sidebar');
            $this->load->view('template/navbar');
            $this->load->view('project/index', $data);
            $this->load->view('template/footerjs');
        }else{
        $company_cd         = $this->input->post('dcompany');
        $project_cd         = $this->input->post('dcode');
        $project_nm         = $this->input->post('dname');
        $project_desc       = $this->input->post('ddesc');
        $start_plan_dt      = $this->input->post('ddate1').$this->input->post('dtime1');
        $finish_plan_dt     = $this->input->post('ddate2').$this->input->post('dtime2');
        $start_actual_dt    = $this->input->post('ddate3').$this->input->post('dtime3');
        $finish_actul_dt    = $this->input->post('ddate4').$this->input->post('dtime4');
        $project_sts        = $this->input->post('dstatus');

        $data = [
            'company_cd'        => $company_cd,
            'project_cd'        => $project_cd,
            'project_nm'        => $project_nm,
            'project_desc'      => $project_desc,
            'start_plan_dt'     => date('y-m-d h-i-s', strtotime($start_plan_dt)),
            'finish_plan_dt'    => date('y-m-d h-i-s', strtotime($finish_plan_dt)),
            'start_actual_dt'   => date('y-m-d h-i-s', strtotime($start_actual_dt)),
            'finish_actul_dt'   => date('y-m-d h-i-s', strtotime($finish_actul_dt)),
            'project_sts'       => $project_sts,
            'created_by'        => 'SYSTEM'
        ];

        $this->m_projects->input_data($data);
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">Success!
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>');
        redirect('projects');
        
        }
    }

    public function edit_form($id)
    {
        $where = ['project_cd' => $id];
        $data['project'] = $this->m_projects->edit($where, 'tb_r_project')->result();
        $data['perusahaan'] = $this->m_projects->getPerusahaan()->result();

        $this->load->view('template/headercss');
        $this->load->view('template/sidebar');
        $this->load->view('template/navbar');
        $this->load->view('project/form_edit', $data);
        $this->load->view('template/footerjs');
    }

    public function edit_aksi($id)
    {
        $company_cd         = $this->input->post('dcompany');
        $project_cd         = $this->input->post('dcode');
        $project_nm         = $this->input->post('dname');
        $project_desc       = $this->input->post('ddesc');
        $start_plan_dt      = $this->input->post('dstartpl');
        $finish_plan_dt     = $this->input->post('dfinishpl');
        $start_actual_dt    = $this->input->post('dstartact');
        $finish_actual_dt   = $this->input->post('dfinishact');
        $project_sts        = $this->input->post('dstatus');

        $data = [
            'company_cd'        => $company_cd,
            'project_cd'        => $project_cd,
            'project_nm'        => $project_nm,
            'project_desc'      => $project_desc,
            'start_plan_dt'     => $start_plan_dt,
            'finish_plan_dt'    => $finish_plan_dt,
            'start_actual_dt'   => $start_actual_dt,
            'finish_actul_dt'   => $finish_actual_dt,
            'project_sts'       => $project_sts,
            'changed_by'        => 'SYSTEM'
        ];

        $where = ['project_cd' => $id];
        $this->m_projects->update($where,$data, 'tb_r_project');
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">Success!
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>');
        redirect('projects');
    }

    public function hapus_aksi($id)
    {
        $where = ['project_cd' => $id];

        $this->m_projects->hapus_data($where, 'tb_r_project');
        $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Deleted!
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>');
        redirect('projects');
    }
}
