<?php

class Resource extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_resource');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['judul'] = 'Dashboard';
        $data['resource'] = $this->m_resource->getAllResource();
        $data['companydata'] = $this->m_resource->company_data()->result();
        $data['autonumber'] = $this->m_resource->auto_number();


        $this->load->view('template/headercss', $data);
        $this->load->view('template/sidebar');
        $this->load->view('template/navbar');
        $this->load->view('resource/index', $data);
        $this->load->view('template/footerjs');
    }
    public function tambah()
    {
        $data['judul'] = 'Add Data';
        $data['kodeunik'] = $this->m_resource->buat_kode();

        $this->form_validation->set_rules('resource_nm', 'name', 'required');
        $this->form_validation->set_rules('resource_type', 'type', 'required');
        $this->form_validation->set_rules('created_dt', 'created date', 'required');
        $this->form_validation->set_rules('created_by', 'created by', 'required');


        if ($this->form_validation->run() == false) {

            $this->load->view('template/headercss', $data);
            $this->load->view('template/sidebar');
            $this->load->view('template/navbar');
            $this->load->view('resource/index', $data);
            $this->load->view('template/footerjs');
        } else {
            $this->m_resource->addData();
            $this->session->set_flashdata('flash', 'Added');

            redirect('Resource');
        }
    }
    public function add()
    {
        $data['autonumber'] = $this->m_resource->auto_number();
        $data['resource'] = $this->m_resource->getAllResource();
        $data['companydata'] = $this->m_resource->company_data()->result();

        $this->form_validation->set_rules('resource_nm', 'name', 'required');


        if ($this->form_validation->run() == false) {

            $this->session->set_flashdata('fail', 'Fail!');
            $this->load->view('template/headercss', $data);
            $this->load->view('template/sidebar');
            $this->load->view('template/navbar');
            $this->load->view('resource/index', $data);
            $this->load->view('template/footerjs');
        } else {
            $this->m_resource->addData();
            $this->session->set_flashdata('flash', 'Added');

            redirect('Resource');
        }
    }
    public function hapus($resource_cd)
    {
        $this->m_resource->deleteData($resource_cd);
        $this->session->set_flashdata('flash', 'Deleted!');
        redirect('Resource');
    }
    public function edit($resource_cd)
    {
        $data['judul'] = 'Edit Data';
        $data['resource'] = $this->m_resource->getResourceById($resource_cd);
        $data['companydata'] = $this->m_resource->company_data()->result();
        $data['resource_nm'] = ['PM', 'PG'];

        $this->form_validation->set_rules('resource_nm', 'Resource_nm', 'required');
        $this->form_validation->set_rules('created_by', 'created_by', 'required');
        $this->form_validation->set_rules('changed_by', 'changed_by', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('template/headercss', $data);
            $this->load->view('template/sidebar');
            $this->load->view('template/navbar');
            $this->load->view('resource/edit');
            $this->load->view('template/footerjs');
        } else {
            $this->m_resource->ubahData();
            $this->session->set_flashdata('flash', 'Changed');

            redirect('Resource');
        }
    }
}
