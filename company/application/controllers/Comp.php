<?php

	class Comp extends CI_Controller{

		function __construct() {
			parent:: __construct();
			$this->load->model('m_comp');
		}

		public function index() {
			$this->load->view('template/headercss');
			$this->load->view('template/sidebar');
			$this->load->view('template/navbar');
			$this->load->view('template/footerjs');
		}

		public function data() {
			$data['datacomp'] = $this->m_comp->read_data()->result();
        	$data['autonumber'] = $this->m_comp->auto_number();

			$this->load->view('template/headercss');
			$this->load->view('template/sidebar');
			$this->load->view('template/navbar');
			$this->load->view('comp/data', $data);
			$this->load->view('template/footerjs');
		}

		public function create() {
			$this->form_validation->set_rules('company_nm', 'Name', 'required');

			if ($this->form_validation->run() == false) {
				$data['datacomp'] = $this->m_comp->read_data()->result();
        		$data['autonumber'] = $this->m_comp->auto_number();

				$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Fail!
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    <span aria-hidden="true">&times;</span>
					  </button>
					</div>');
				$this->load->view('template/headercss');
				$this->load->view('template/sidebar');
				$this->load->view('template/navbar');
				$this->load->view('comp/data', $data);
				$this->load->view('template/footerjs');
			}else{
				$company_cd = $this->input->post('company_cd');
				$company_nm = $this->input->post('company_nm');
				$brand_cd = $this->input->post('brand_cd');

				$data = [
					'company_cd' => $company_cd,
					'company_nm' => $company_nm,
					'branch_cd' => $brand_cd,
					'created_by' => 'SYSTEM',
				];

				$this->m_comp->create_data($data, 'tb_m_company');
				$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">Success!
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    <span aria-hidden="true">&times;</span>
					  </button>
					</div>');
				Redirect('Comp/data');
			}

			
		}

		public function delete($id) {
			$id = ['company_cd' => $id];
			$this->m_comp->delete_data($id, 'tb_m_company');
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">Success!
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>');
			Redirect('Comp/data');
		}

		public function updatedata($id) {
			$id = ['company_cd' => $id];
			$data['datacomp'] = $this->m_comp->update_data($id, 'tb_m_company')->result();

			$this->load->view('template/headercss');
			$this->load->view('template/sidebar');
			$this->load->view('template/navbar');
			$this->load->view('comp/update', $data);
			$this->load->view('template/footerjs');
		}

		public function update($id) {
			$id = ['company_cd' => $id];
			$company_cd = $this->input->post('company_cd');
			$company_nm = $this->input->post('company_nm');
			$brand_cd = $this->input->post('brand_cd');

			$data = [
				'company_cd' => $company_cd,
				'company_nm' => $company_nm,
				'branch_cd' => $brand_cd,
				'changed_by' => 'SYSTEM',
			];

			$this->m_comp->update($id, $data, 'tb_m_company');
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">Success!
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>');
			Redirect('Comp/data');
		}

		public function search() {
			$keyword = $this->input->get('keyword');
			$data['datacomp'] = $this->m_comp->get_keyword($keyword, 'tb_m_company');

			$this->load->view('template/headercss');
			$this->load->view('template/sidebar');
			$this->load->view('template/navbar');
			$this->load->view('comp/data', $data);
			$this->load->view('template/footerjs');
		}

	}

?>		