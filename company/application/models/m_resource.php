<?php

class m_resource extends CI_Model
{
    public function auto_number()
    {
        $this->db->select('RIGHT(tb_m_resource.resource_cd,1) as kode', FALSE);
        $this->db->order_by('resource_cd', 'DESC');
        $this->db->limit(1);

        $query = $this->db->get('tb_m_resource');
        if ($query->num_rows() <> 0) {
            //jika kode ada
            $data = $query->row();
            $kode = intval($data->kode) + 1;
        } else {
            //jika belum ada
            $kode = 1;
        }

        $kodemax = str_pad($kode, 2, "0", STR_PAD_LEFT); //4 = JUMLAH DIGIT
        $kodejadi = "RES" . $kodemax; //hasil jadi PRCxxxxx
        return $kodejadi;
    }
    public function company_data()
    {
        return $this->db->get('tb_m_company');
    }

    public function getAllResource()
    {
        return $this->db->get('tb_m_resource')->result_array();
    }
    public function addData()
    {
        $this->load->helper('date');
        $data = [
            "resource_cd" => $this->input->post('resource_cd', true),
            "resource_nm" => $this->input->post('resource_nm', true),
            "company_cd" => $this->input->post('company_cd', true),
            "created_dt" => $this->input->post('created_dt', true),
            "created_by" => 'SYSTEM',
            "changed_dt" => '',
            "changed_by"  => ''
        ];
        $this->db->insert('tb_m_resource', $data);
    }
    public function deleteData($resource_cd)
    {
        $this->db->where('resource_cd', $resource_cd);
        $this->db->delete('tb_m_resource');
    }
    public function getResourceById($resource_cd)
    {
        return $this->db->get_where('tb_m_resource', ['resource_cd' => $resource_cd])->row_array();
    }
    public function ubahData()
    {
        $this->load->helper('date');
        $data = [

            "resource_nm" => $this->input->post('resource_nm', true),
            "company_cd" => $this->input->post('company_cd', true),
            "created_dt" => $this->input->post('created_dt', true),
            "created_by" => $this->input->post('created_by', true),
            "changed_dt" => $this->input->post('changed_dt', true),
            "changed_by"  => $this->input->post('changed_by', true)

        ];
        $this->db->where('resource_cd', $this->input->post('resource_cd'));
        $this->db->update('tb_m_resource', $data);
    }
    public function buat_kode()
    {
        $this->db->select('RIGHT(tb_m_resource.resource_cd,2) as kode', FALSE);
        $this->db->order_by('resource_cd', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get('tb_m_resource');
        if ($query->num_rows() <> 0) {
            //jika kodenya ada
            $data = $query->row();
            $kode = intval($data->kode) + 1;
        } else {
            //jika kodenya belum ada
            $kode = 1;
        }

        $kodemax = str_pad($kode, 2, "0", STR_PAD_LEFT);
        $kodejadi = "arka" . $kodemax;
        return $kodejadi;
    }
}
