<?php

	class M_comp extends CI_Model{

		public function auto_number() {
			$this->db->select('RIGHT(tb_m_company.company_cd,4) as kode', FALSE);
			$this->db->order_by('company_cd','DESC');
			$this->db->limit(1);

			$query = $this->db->get('tb_m_company');
			if ($query->num_rows() <> 0) {
				//jika kode ada
				$data = $query->row();
				$kode = intval($data->kode) +1 ;
			}else{
				//jika belum ada
				$kode = 1;
			}

			$kodemax = str_pad($kode,5 ,"0", STR_PAD_LEFT); //4 = JUMLAH DIGIT
			$kodejadi = "MC".$kodemax; //hasil jadi PRCxxxxx
			return $kodejadi;
		}

		public function read_data() {
			return $this->db->get('tb_m_company');
		}

		public function create_data($data, $table) {
			$this->db->insert($table, $data);
		}

		public function delete_data($id, $table) {
			$this->db->where($id);
			$this->db->delete($table);
		}

		public function update_data($id, $table) {
			return $this->db->get_where($table, $id);
		}

		public function update($id, $data, $table) {
			$this->db->where($id);
			$this->db->update($table, $data);
		}

		public funcTion get_keyword($keyword, $table) {
			$this->db->select('*');
			$this->db->from($table);
			$this->db->like('company_cd', $keyword);
			$this->db->or_like('company_nm', $keyword);
			$this->db->or_like('branch_cd', $keyword);
			return $this->db->get()->result();
		}

	}

?>