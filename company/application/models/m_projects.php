<?php
class M_projects extends CI_Model
{
	public function auto_number()
	{
		$this->db->select('RIGHT(tb_r_project.project_cd,4) as kode', FALSE);
		$this->db->order_by('project_cd','DESC');
		$this->db->limit(1);

		$query = $this->db->get('tb_r_project');
		if ($query->num_rows() <> 0) {
			//jika kode ada
			$data = $query->row();
			$kode = intval($data->kode) +1 ;
		}else{
			//jika belum ada
			$kode = 1;
		}

		$kodemax = str_pad($kode,5 ,"0", STR_PAD_LEFT); //4 = JUMLAH DIGIT
		$kodejadi = "PRC".$kodemax; //hasil jadi PRCxxxxx
		return $kodejadi;
	}

    public function tampil_data()
    {
        return $this->db->get('tb_r_project');
    }

    public function tambah_data()
    {
        return $this->db->get('tb_m_company');
    }

    public function input_data($data)
    {
    	$this->db->insert('tb_r_project', $data);
    }

    public function edit($where , $table)
    {
    	return $this->db->get_where($table , $where);
    }

    function getPerusahaan()
    {
        $this->db->join('tb_m_company', 'tb_m_company.company_cd = tb_r_project.company_cd', 'left');
        $this->db->select('*');
        return $this->db->get('tb_r_project');
    }

    public function update($where , $data, $table)
    {
  		$this->db->where($where);
  		$this->db->update($table, $data);
    }

    public function hapus_data($table,$where)
    {
    	$this->db->delete($where , $table);
    }
}
