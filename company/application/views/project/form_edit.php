    <?php foreach ($project as $d) : ?>
      <?= form_open_multipart('projects/edit_aksi/'.$d->project_cd); ?>
           <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Data Update Company</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="form-group row">
              <label class="col-sm-2 form-control-label ">Company Name</label>
              <div class="col-sm-5">
                <select class="form-control col-sm-9" id="exampleFormControlSelect1" name="dcompany">
                  <?php foreach ($perusahaan as $j) : ?>
                      
                    <option value="<?= $j->company_cd; ?>" ><?= $j->company_cd; ?></option>
                    
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="exampleFormControlInput1">Project Code</label>
              <input type="text" class="form-control"  placeholder="<?= $d->project_cd ?>" name="dcode" value="<?= $d->project_cd ?>" readonly>
            </div>
            <div class="form-group">
              <label for="exampleFormControlSelect2">Project Name</label>
              <input type="text" class="form-control col-sm-3"  placeholder="Insert Project name..." name="dname" value="<?= $d->project_nm ?>">
            </div>
            <div class="form-group">
              <label class="form-control-label ">Project Description</label>
              <input type="text" class="form-control"  placeholder="Insert Project description..." name="ddesc" value="<?= $d->project_desc ?>">
            </div>
            <div class="form-group">
              <label class="form-control-label ">Start Plan</label>
              <input type="text" class="form-control"  placeholder="Insert Project description..." name="dstartpl" value="<?= $d->start_plan_dt ?>">
            </div>
            <div class="form-group">
              <label class="form-control-label ">Finish Plan</label>
              <input type="text" class="form-control"  placeholder="Insert Project description..." name="dfinishpl" value="<?= $d->finish_plan_dt ?>">
            </div>
            <div class="form-group">
              <label class="form-control-label ">Start Actual</label>
              <input type="text" class="form-control"  placeholder="Insert Project description..." name="dstartact" value="<?= $d->start_actual_dt ?>">
            </div>
            <div class="form-group">
              <label class="form-control-label ">Finish Actual</label>
              <input type="text" class="form-control"  placeholder="Insert Project description..." name="dfinishact" value="<?= $d->finish_actul_dt ?>">
            </div>
            <div class="form-group">
              <label for="exampleFormControlSelect2">Project Status</label>
              <input type="text" class="form-control col-sm-3"  placeholder="Insert Project status..." name="dstatus" value="<?= $d->project_sts ?>">
            </div>
        </div>
      <div class="modal-footer">
        <a href="<?= base_url('projects') ?>" class="btn btn-secondary" data-dismiss="modal">Back</a>
        <button onclick='javascript : return confirm("Update Data ?")' type="submit" class="btn btn-warning">Update</button>
      </div>
    </div>
    <?= form_close(); ?>
    <?php endforeach; ?>