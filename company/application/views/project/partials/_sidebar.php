<!-- Sidebar -->
    <ul class="sidebar navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="index.html">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url("comp")?>">
          <i class="fas fa-fw fa-university"></i>
          <span>Company</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url("home")?>">
          <i class="fas fa-fw fa-folder"></i>
          <span>Resource</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url("projects")?>">
          <i class="fas fa-fw fa-tasks"></i>
          <span>Projects</span></a>
      </li>
    </ul>

    <div id="content-wrapper">

      <div class="container-fluid">
