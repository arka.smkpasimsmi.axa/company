<?= $this->session->flashdata('message'); ?>
 <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Data Table Project</h1>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Data Table</h6>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-lg-9">
                </div>
                <div class="col-lg-3">
                  <form action="<?= base_url('Comp/search') ?>" method="get">
                    <div class="input-group mb-2">
                      <div class="input-group-prepend">
                        <label class="input-group-text" for="submitsearch" style="cursor: pointer;">Search</label>
                      </div>
                      <input type="text" name="keyword" class="form-control" placeholder="Search . . ." autocomplete="off">
                      <button type="submit" id="submitsearch" style="display: none;"></button>
                    </div>  
                  </form>
                </div> 
              </div>
               <label class="sr-only" for="inlineFormInputGroup">Username</label>
                
              <div class="table-responsive">
            <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
              <thead>
                <tr role="row">
                  <th>No</th>
                  <th>Company Code</th>
                  <th>Project Code</th>
                  <th>Project Name</th>
                  <th>Project Description</th>
                  <th>Start Plan</th>
                  <th>Finish Plan</th>
                  <th>Start Actual</th>
                  <th>Finish Actual</th>
                  <th>Project Status</th>
                  <th>Created Date</th>
                  <th>Created By</th>
                  <th>Changed Date</th>
                  <th>Changed By</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tfoot>

              </tfoot>
              <?php $no = 1;
              foreach ($project as $d) : ?>
                <tbody>
                  <tr role="row" class="odd">
                    <td><?= $no++ ?></td>
                    <td><?= $d->company_cd ?></td>
                    <td><?= $d->project_cd ?></td>
                    <td><?= $d->project_nm ?></td>
                    <td><?= $d->project_desc ?></td>
                    <td><?= $d->start_plan_dt ?></td>
                    <td><?= $d->finish_plan_dt ?></td>
                    <td><?= $d->start_actual_dt ?></td>
                    <td><?= $d->finish_actul_dt ?></td>
                    <td><?= $d->project_sts ?></td>
                    <td><?= $d->created_dt ?></td>
                    <td><?= $d->created_by ?></td>
                    <td><?= $d->changed_dt ?></td>
                    <td><?= $d->changed_by ?></td>
                    <td><?= anchor('projects/edit_form/' . $d->project_cd, '<div class="btn btn-outline-warning btn-sm"><i class="fa fa-pen"></i></div>') ?>
                      <a onclick='javascript : return confirm("Delete selected Data ?")' class="btn btn-outline-danger btn-sm" href="<?= base_url('projects/hapus_aksi/' . $d->project_cd) ?>"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                </tbody>
              <?php endforeach; ?>
            </table>
          </div>
        </div>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                Create
            </button>
      </div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
        <?= form_open_multipart('Projects/tambah_aksi'); ?>
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create New Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="form-group">
              <label class="form-control-label ">Company Name</label>
                <select class="form-control" id="exampleFormControlSelect1" name="dcompany">
                  <?php foreach ($namaperusahaan as $d) : ?>
                    <option value="<?= $d->company_cd; ?>"><?= $d->company_nm; ?></option>
                  <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
              <label for="exampleFormControlInput1">Project Code</label>
              <input type="text" class="form-control"  placeholder="<?= $autonumber?>" name="dcode" value="<?= $autonumber?>"readonly>
            </div>
            <div class="form-group">
              <label for="exampleFormControlSelect2">Project Name</label>
              <input type="text" class="form-control"  placeholder="Insert Project name..." name="dname"value="<?= set_value('dname') ?>" >
              <small class="text-danger"><?= form_error('dname') ?></small>
            </div>
            <div class="form-group">
              <label class="form-control-label ">Project Description</label>
              <input type="text" class="form-control"  placeholder="Insert Project description..." name="ddesc"value="<?= set_value('ddesc') ?>" >
              <small class="text-danger"><?= form_error('ddesc') ?></small>
            </div>
            <label class="form-control-label">Start Plan</label>
            <div class="form-row ml-5">
              <div class="form-group col-md-5">
                <label for="inputEmail4">Date</label>
                <input type="date" class="form-control" name="ddate1"value="<?= set_value('ddate1') ?>">
                <small class="text-danger"><?= form_error('ddate1') ?></small>
              </div>
              <div class="form-group col-md-5">
                <label for="inputPassword4">Time</label>
                <input type="time" class="form-control" name="dtime1"value="<?= set_value('dtime1') ?>">
                <small class="text-danger"><?= form_error('dtime1') ?></small>
              </div>
            </div>
            <label class="form-control-label">Finish Plan</label>
            <div class="form-row ml-5">
              <div class="form-group col-md-5">
                <label for="inputEmail4">Date</label>
                <input type="date" class="form-control" name="ddate2"value="<?= set_value('ddate2') ?>">
                <small class="text-danger"><?= form_error('ddate2') ?></small>
              </div>
              <div class="form-group col-md-5">
                <label for="inputPassword4">Time</label>
                <input type="time" class="form-control" name="dtime2"value="<?= set_value('dtime2') ?>">
                <small class="text-danger"><?= form_error('dtime2') ?></small>
              </div>
            </div>
            <label class="form-control-label">Start Actual</label>
            <div class="form-row ml-5">
              <div class="form-group col-md-5">
                <label for="inputEmail4">Date</label>
                <input type="date" class="form-control" name="ddate3"value="<?= set_value('ddate3') ?>">
                <small class="text-danger"><?= form_error('ddate3') ?></small>
              </div>
              <div class="form-group col-md-5">
                <label for="inputPassword4">Time</label>
                <input type="time" class="form-control" name="dtime3"value="<?= set_value('dtime3') ?>">
                <small class="text-danger"><?= form_error('dtime3') ?></small>
              </div>
            </div>
            <label class="form-control-label">Finish Actual</label>
            <div class="form-row ml-5">
              <div class="form-group col-md-5">
                <label for="inputEmail4">Date</label>
                <input type="date" class="form-control" name="ddate4"value="<?= set_value('ddate4') ?>">
                <small class="text-danger"><?= form_error('ddate4') ?></small>
              </div>
              <div class="form-group col-md-5">
                <label for="inputPassword4">Time</label>
                <input type="time" class="form-control" name="dtime4"value="<?= set_value('dtime4') ?>">
                <small class="text-danger"><?= form_error('dtime4') ?></small>
              </div>
            </div>
            <div class="form-group">
              <label for="exampleFormControlSelect2">Project Status</label>
              <input type="text" class="form-control"  placeholder="Insert Project status..." name="dstatus"value="<?= set_value('dstatus') ?>" >
              <small class="text-danger"><?= form_error('dstatus') ?></small>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Create</button>
      </div>
    </div>
        <?= form_close(); ?>
  </div>
</div>

