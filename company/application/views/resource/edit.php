 <div class="modal-content">
     <div class="modal-header">
         <h5 class="modal-title" id="exampleModalLabel">Data Update Resource</h5>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true">&times;</span>
         </button>
     </div>
     <div class="modal-body">
         <form action="" method="post">
             <div class="form-group">
                 <label for="cari">Resource Code</label>
                 <input type="text" name="resource_cd" value="<?= $resource['resource_cd']; ?>" class="form-control" id="kodebarang" readonly>
             </div>
             <div class="form-group">
                 <label for="nama">Resource Name</label>
                 <input type="text" name="resource_nm" class="form-control" id="resource_nm" value="<?= $resource['resource_nm']; ?>">
                 <small class="form-text text-danger"><?= form_error('resource_nm'); ?></small>
             </div>
             <div class="form-group">
                 <label for="nama">Company Code</label>
                 <select class="form-control" id="company_cd" name="company_cd">
                     <?php foreach ($companydata as $data) : ?>
                         <option value="<?= $data->company_cd; ?>"><?= $data->company_cd; ?></option>
                     <?php endforeach; ?>
                     <small class="form-text text-danger"><?= form_error('resource_nm'); ?></small>
                 </select>
             </div>
             <div class="form-group">
                 <input type="hidden" name="created_dt" class="form-control" id="created_dt" value="<?= $resource['created_dt']; ?>" disabled>
                 <small class="form-text text-danger"><?= form_error('created_dt'); ?></small>
             </div>
             <div class="form-group">
                 <input type="hidden" name="created_by" class="form-control" id="created_by" value="SYSTEM">
                 <small class="form-text text-danger"><?= form_error('created_by'); ?></small>
             </div>
             <div class="form-group">
                 <input type="hidden" name="changed_dt" class="form-control" id="changed_dt" value="<?= @date('Y-m-d H:i:s'); ?>" disabled>
                 <small class="form-text text-danger"><?= form_error('changed_dt'); ?></small>
             </div>
             <div class="form-group">
                 <input type="hidden" name="changed_by" class="form-control" id="changed_by" value="SYSTEM">
                 <small class="form-text text-danger"><?= form_error('changed_by'); ?></small>
             </div>
             <div class="modal-footer">
                <a href="<?= base_url('Resource') ?>" class="btn btn-secondary" data-dismiss="modal">Back</a>
                <button type="submit" class="btn btn-warning" onclick="return confirm('Are You sure?')">Update</button>
              </div>
         </form>
     </div>