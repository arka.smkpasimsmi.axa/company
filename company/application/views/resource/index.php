  <?php if ($this->session->flashdata('flash')) : ?>
    <div class="alert alert-success alert-success fade show" role="alert">Data
      <strong>Has </strong><?php echo $this->session->flashdata('flash'); ?>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  <?php endif; ?>

  <?php if ($this->session->flashdata('fail')) : ?>
    <div class="alert alert-danger alert-success fade show" role="alert">Data
      <strong>Has </strong><?php echo $this->session->flashdata('fail'); ?>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  <?php endif; ?>

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Data Table Resource</h1>
  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Data Table</h6>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-lg-9">
        </div>
        <div class="col-lg-3">
          <form action="<?= base_url('Comp/search') ?>" method="get">
            <div class="input-group mb-2">
              <div class="input-group-prepend">
                <label class="input-group-text" for="submitsearch" style="cursor: pointer;">Search</label>
              </div>
              <input type="text" name="keyword" class="form-control" placeholder="Search . . ." autocomplete="off">
              <button type="submit" id="submitsearch" style="display: none;"></button>
            </div>
          </form>
        </div>
      </div>
      <label class="sr-only" for="inlineFormInputGroup">Username</label>

      <div class="table-responsive">
        <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
          <thead>
            <tr>
              <th>No</th>
              <th>Resource Code</th>
              <th>Resource Name</th>
              <th>Company Code</th>
              <th>Created Date</th>
              <th>Created By</th>
              <th>Changed Date</th>
              <th>Changed By</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php $no = 1;
            foreach ($resource as $r) : ?>
              <tr>
                <td><?= $no++ ?></td>
                <td><?= $r['resource_cd']; ?></td>
                <td><?= $r['resource_nm']; ?></td>
                <td><?= $r['company_cd']; ?></td>
                <td><?= $r['created_dt']; ?></td>
                <td><?= $r['created_by']; ?></td>
                <td><?= $r['changed_dt']; ?></td>
                <td><?= $r['changed_by']; ?></td>
                <td>
                  <a href="<?= base_url(); ?>Resource/edit/<?= $r['resource_cd']; ?>" class="btn btn-outline-warning btn-sm"><i class="fas fa-pen"></i></a>
                  <a href="<?= base_url(); ?>Resource/hapus/<?= $r['resource_cd']; ?>" class="btn btn-outline-danger btn-sm" onclick="return confirm('Are You sure?');"><i class="far fa-trash-alt"></i></a>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
      Create
    </button>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <?= form_open_multipart('Resource/add'); ?>
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Create New Data</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="cari">Resource Code</label>
            <input type="text" name="resource_cd" class="form-control" id="kodebarang" value="<?= $autonumber; ?>" readonly>
          </div>
          <div class="form-group">
            <label for="nama">Resource Name</label>
            <input type="text" name="resource_nm" class="form-control" id="resource_nm" placeholder="Resource Name...">
            <small class="form-text text-danger"><?= form_error('resource_nm'); ?></small>
          </div>
          <div class="form-group">
            <label for="jurusan">Company Code</label>
            <select class="form-control" id="resource_type" name="company_cd">
              <?php foreach ($companydata as $data) : ?>
                <option value="<?= $data->company_cd; ?>"><?= $data->company_cd; ?></option>
              <?php endforeach; ?>
            </select>
          </div>
          <div class="form-group">
            <!-- <label for="nama">Created Date</label> -->
            <input type="text" name="created_dt" class="form-control" id="created_dt" value="<?= @date('Y-m-d H:i:s'); ?>" hidden>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Create</button>
        </div>
      </div>
      <?= form_close(); ?>
    </div>
  </div>