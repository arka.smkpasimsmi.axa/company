  <?php foreach($datacomp as $data) : ?>
    <?= form_open_multipart('Comp/update/'.$data->company_cd); ?>
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Data Update Company</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="company_cd">Company Code</label>
            <input type="text" class="form-control" name="company_cd" id="company_cd" autocomplete="off" placeholder="Company Code" value="<?= $data->company_cd ?>" readonly>
          </div>
          <div class="form-group">
            <label for="company_nm">Company Name</label>
            <input type="text" class="form-control" name="company_nm" id="company_nm" autocomplete="off" placeholder="Company Name" value="<?= $data->company_nm ?>">
          </div>
          <div class="form-group">
            <label for="branch_cd">Branch Code</label>
            <input type="text" class="form-control" name="branch_cd" id="branch_cd" autocomplete="off" placeholder="Branch Code" value="<?= $data->branch_cd ?>">
          </div>
        </div>
      <div class="modal-footer">
        <a href="<?= base_url('Comp/data') ?>" class="btn btn-secondary" data-dismiss="modal">Back</a>
        <button type="submit" class="btn btn-warning" onclick="return confirm('Are You sure?')">Update</button>
      </div>
    </div>
    <?= form_close(); ?>
  <?php endforeach; ?>