<?= $this->session->flashdata('message'); ?>
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Data Table Company</h1>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Data Table</h6>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-lg-9">
                </div>
                <div class="col-lg-3">
                  <form action="<?= base_url('Comp/search') ?>" method="get">
                    <div class="input-group mb-2">
                      <div class="input-group-prepend">
                        <label class="input-group-text" for="submitsearch" style="cursor: pointer;">Search</label>
                      </div>
                      <input type="text" name="keyword" class="form-control" placeholder="Search . . ." autocomplete="off">
                      <button type="submit" id="submitsearch" style="display: none;"></button>
                    </div>  
                  </form>
                </div> 
              </div>
               <label class="sr-only" for="inlineFormInputGroup">Username</label>
                
              <div class="table-responsive">
                <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                  <thead>
                    <tr role="row" align="center">
                      <th>No</th>
                      <th>Company Code</th>
                      <th>Company Name</th>
                      <th>Branch Code</th>
                      <th>Created Date</th>
                      <th>Created By</th>
                      <th>Changed Date</th>
                      <th>Changed By</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <!-- <tfoot>
                    <tr align="center"><th rowspan="1" colspan="1">No</th><th rowspan="1" colspan="1">Company Code</th><th rowspan="1" colspan="1">Company Name</th><th rowspan="1" colspan="1">Branch Code</th><th rowspan="1" colspan="1">Action</th>
                  </tfoot> -->
                  <tbody>
                    <?php $no = 1; foreach($datacomp as $data) : ?>
                      <tr role="row" class="odd" align="center">
                        <td><?= $no++; ?></td>
                        <td class="sorting_1"><?= $data->company_cd; ?></td>
                        <td><?= $data->company_nm ?></td>
                        <td><?= $data->branch_cd ?></td>
                        <td><?= $data->created_dt ?></td>
                        <td><?= $data->created_by ?></td>
                        <td><?= $data->changed_dt ?></td>
                        <td><?= $data->changed_by ?></td>
                        <td>
                          <a href="<?= base_url('Comp/updatedata/').$data->company_cd ?>" class="btn btn-outline-warning btn-sm">
                            <i class="fas fa-pen"></i></a>
                          <a href="<?= base_url('Comp/delete/').$data->company_cd ?>" class="btn btn-outline-danger btn-sm" onclick="return confirm('Are You sure?')">
                            <i class="fas fa-trash-alt"></i></a>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
              Create
            </button>
          </div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
        <?= form_open_multipart('Comp/create'); ?>
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create New Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="form-group">
            <label for="company_cd">Company Code</label>
            <input type="text" class="form-control" name="company_cd" id="company_cd" placeholder="Company Code" autocomplete="off" value="<?= $autonumber; ?>" readonly>
          </div>
          <div class="form-group">
            <label for="company_nm">Company Name</label>
            <input type="text" class="form-control" name="company_nm" id="company_nm" placeholder="Company Name" autocomplete="off" value="<?= set_value('company_nm') ?>">
            <small class="text-danger"><?= form_error('company_nm') ?></small>
          </div>
          <div class="form-group">
            <label for="branch_cd">Branch Code</label>
            <input type="text" class="form-control" name="branch_cd" id="branch_cd" placeholder="Branch Code" autocomplete="off">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Create</button>
      </div>
    </div>
        <?= form_close(); ?>
  </div>
</div>